package deckdriver;

public class DeckDriver {

    public static void main(String[] args) {
        Deck newDeck = new Deck();
        int testCases = 0;
        int inBetween = 0;
        int notInBetween = 0;
        int matchesCard = 0;
        for (int i = 0; i < 3; i++) {
            testCases++;
            System.out.println("Test Case #" + testCases);
            newDeck.shuffle();
            Card card1 = newDeck.deal();
            System.out.println("Card 1: " + card1);
            Card card2 = newDeck.deal();
            System.out.println("Card 2: " + card2);
            if (card1.compareTo(card2) == 0) {
                System.out.println("Sorry, these two cards are equal, let's do this again..");
                System.out.println("");
                continue;
            }
            Card card3 = newDeck.deal();
            System.out.println("Card 3 (your card): " + card3);
            if (card3.compareTo(card1) == 0 || card3.compareTo(card2) == 0) {
                System.out.println("Unlucky! double your loss");
                matchesCard++;
            } else if ((card3.compareTo(card1) == 1 && card3.compareTo(card2) == -1) ||
                    (card3.compareTo(card1) == -1 && card3.compareTo(card2) == 1)) {
                System.out.println("Congratz, your card was between card 1 and 2");
                inBetween++;
            } else {
                System.out.println("sorry, your card was not between both");
                notInBetween++;
            } 
            System.out.println("");
            
        }
        System.out.println("");
        System.out.println("Stats: ");
        System.out.println("Number of comparisons: " + (matchesCard + notInBetween + inBetween));
        System.out.println("Number of wins: " + inBetween);
        System.out.println("Number of loses: " + (notInBetween + matchesCard));
        System.out.println("    not inbetween: " + notInBetween + " card matches: " + matchesCard);
        
    }

}
