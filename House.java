package deckdriver;

/**
 *
 * @author robma
 */
public class House {
    public double pot; // amount of the pot in play
    public Deck mainDeck = new Deck(); // the card deck being used
    
    /**
     * constructor that recieves the amount of the buyin and sets the pot
     * (number of players times buyin)
     * @param buyIn
     * @param playerCount 
     */
    public House(int buyIn, int playerCount) {  // To Be Implemented/WIP
        pot = buyIn * playerCount;
    }
    
    /**
     * PlayHand receives the current player. it should only receive active players
     */
    public void PlayHand() {   // To Be Implemented
        Card card1;
        Card card2;
        Card card3;
        do {
            card1 = mainDeck.deal();
            card2 = mainDeck.deal();
            if (card1.compareTo(card2) == 0) {
                // display something that says the cards are the same thus we are re-dealing
            }
        } while (card1.compareTo(card2) != 0);
        
        // get bet
        // deal third card
        card3 = mainDeck.deal();
        // adjust pot and players bankroll depending on results
        if (card3.compareTo(card1) == 0 || card3.compareTo(card2) == 0) {
            // double loss
        } else if ((card3.compareTo(card1) == 1 && card3.compareTo(card2) == -1) ||
                (card3.compareTo(card1) == -1 && card3.compareTo(card2) == 1)) {
            //winning hand
        } else {
            // regular losing hand
        } 
        
    }
    
    /**
     * asks player for a bet. it will keep asking until a valid bet is placed.
     * must be able to handle an illegalbet exception (TRY CATCH)
     * @param player
     * @return valid bet
     */
    public double getBet(Person player) { // To Be Implemented
        return 0;
    }
}
