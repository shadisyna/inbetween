package deckdriver;

/**
 *
 * @author robma
 */
public class Person {
    public String name; // The name of the person playing
    public double bankroll;  // The amount the person has to play
    public double bet;  // The amount a player bets on a hand (Can't exceed bankroll)
    public boolean activePlayer; // Boolean that tells if the player is still in the game

    /**
     * Constructor. Builds Object, passed name and initial bankroll
     * @param name
     * @param bankroll 
     */
    public Person(String name, Double bankroll) {
        this.name = name;
        this.bankroll = bankroll;
    }
    
    /**
     * setBankroll handles winning hands and losing hands;
     * winning hands take a bet amount from pot and adds it to players bankroll
     * losing hands deduct bet amount from players bankroll and adds to pot
     * (keep in mind double loss from matching cards)
     * if bankroll is less than or equal to 0, activeplayer is set to false
     */
    public void setBankroll() {  // To Be Implemented
        
    }
    
    /**
     * LeaveGame removes player from game
     */
    public void LeaveGame() {  // To Be Implemented
        activePlayer = false;
    }
    
    /**
     * Reduced bankroll by buy in amount (set from game driver) and sets
     * active player to true
     */
    public void BuyIn() {   // To Be Implemented
        
    }
    
    /**
     * used to save bet placed by the player
     */
    public void BetAmount() {   // To Be Implemented
        
    }

    /**
     * getter for name of player, used to display name in GUI
     * @return name of player
     */
    public String getName() {
        return name;
    }

    /**
     * getter for bankroll of player, used to display balance of player in GUI
     * @return bankroll of player
     */
    public Double getBankroll() {
        return bankroll;
    }

    /**
     * getter for bet
     * @return bet
     */
    public Double getBet() {
        return bet;
    }

    /**
     * getter for if the player is active
     * @return activeplayer status
     */
    public boolean isActivePlayer() {
        return activePlayer;
    }
    
    /**
     * looks to see if two players are the same (kind of useless?)
     * @param other
     * @return 
     */
    public boolean equals(Person other) {   // WIP
        if (other.name == this.name) {
            return true;
        }
        return false;
    }
    
    /**
     * compares bankroll to see who's in lead.
     * @param other 
     */
    public void compareTo(Person other) {   // To Be Implemented
        
    }
}
